var kaunas = [
  {
    a: "54°59′48″N",
    lat: 54.99680085,
    b: "23°52′41″E",
    lng: 23.87803331
  },
  {
    a: "54°44′45″N",
    lat: 54.74575145,
    b: "23°44′47″E",
    lng: 23.74637514
  },
  {
    a: "54°54′31″N",
    lat: 54.9086663,
    b: "23°39′26″E",
    lng: 23.65719608
  },
  {
    a: "55°03′01″N",
    lat: 55.05021355,
    b: "23°59′52″E",
    lng: 23.99788862
  },
  {
    a: "55°01′55″N",
    lat: 55.03200119,
    b: "24°11′49″E",
    lng: 24.19689086
  },
  {
    a: "54°57′45″N",
    lat: 54.96238827,
    b: "23°58′15″E",
    lng: 23.97086716
  },
  {
    a: "55°02′58″N",
    lat: 55.04958223,
    b: "23°38′38″E",
    lng: 23.64390104
  },
  {
    a: "54°52′13″N",
    lat: 54.87038027,
    b: "24°14′41″E",
    lng: 24.24462946
  },
  {
    a: "54°50′20″N",
    lat: 54.83895622,
    b: "24°06′34″E",
    lng: 24.10950001
  },
  {
    a: "54°56′08″N",
    lat: 54.93543418,
    b: "23°52′16″E",
    lng: 23.8709811
  },
  {
    a: "54°53′41″N",
    lat: 54.89479926,
    b: "23°55′02″E",
    lng: 23.91734687
  },
  {
    a: "54°53′10″N",
    lat: 54.88614583,
    b: "24°14′18″E",
    lng: 24.23830859
  },
  {
    a: "54°51′28″N",
    lat: 54.85766702,
    b: "23°44′29″E",
    lng: 23.74145437
  },
  {
    a: "54°43′01″N",
    lat: 54.71690864,
    b: "23°54′38″E",
    lng: 23.91056858
  },
  {
    a: "54°58′23″N",
    lat: 54.97301006,
    b: "24°00′40″E",
    lng: 24.01106409
  },
  {
    a: "55°02′57″N",
    lat: 55.04925886,
    b: "23°45′12″E",
    lng: 23.75339304
  },
  {
    a: "54°50′33″N",
    lat: 54.84243458,
    b: "24°04′10″E",
    lng: 24.06931074
  },
  {
    a: "54°48′38″N",
    lat: 54.81064658,
    b: "23°42′15″E",
    lng: 23.70405193
  },
  {
    a: "54°53′57″N",
    lat: 54.89907755,
    b: "23°48′24″E",
    lng: 23.80670973
  },
  {
    a: "54°49′48″N",
    lat: 54.83003579,
    b: "23°47′20″E",
    lng: 23.78890975
  },
  {
    a: "54°49′31″N",
    lat: 54.82529664,
    b: "23°56′53″E",
    lng: 23.94809002
  },
  {
    a: "55°03′41″N",
    lat: 55.06147698,
    b: "24°01′22″E",
    lng: 24.0227869
  },
  {
    a: "55°00′40″N",
    lat: 55.01102729,
    b: "24°09′21″E",
    lng: 24.15584566
  },
  {
    a: "55°06′48″N",
    lat: 55.11346671,
    b: "23°53′08″E",
    lng: 23.88563594
  },
  {
    a: "54°44′46″N",
    lat: 54.74599686,
    b: "23°50′40″E",
    lng: 23.84432034
  },
  {
    a: "55°01′12″N",
    lat: 55.02007577,
    b: "24°04′24″E",
    lng: 24.07344062
  },
  {
    a: "54°46′01″N",
    lat: 54.76695966,
    b: "23°46′45″E",
    lng: 23.77912749
  },
  {
    a: "54°56′15″N",
    lat: 54.93750362,
    b: "24°15′04″E",
    lng: 24.25124643
  },
  {
    a: "54°49′22″N",
    lat: 54.82287929,
    b: "24°03′41″E",
    lng: 24.06137194
  },
  {
    a: "55°06′26″N",
    lat: 55.10728863,
    b: "23°49′39″E",
    lng: 23.82740687
  },
  {
    a: "54°50′07″N",
    lat: 54.83527977,
    b: "23°41′23″E",
    lng: 23.68962427
  },
  {
    a: "54°46′03″N",
    lat: 54.767464,
    b: "23°55′25″E",
    lng: 23.92364373
  },
  {
    a: "54°54′59″N",
    lat: 54.91660666,
    b: "23°37′47″E",
    lng: 23.62968848
  },
  {
    a: "54°53′34″N",
    lat: 54.89285836,
    b: "23°42′02″E",
    lng: 23.70064108
  },
  {
    a: "54°47′58″N",
    lat: 54.79938443,
    b: "23°52′57″E",
    lng: 23.88250852
  },
  {
    a: "54°51′33″N",
    lat: 54.85928199,
    b: "24°15′21″E",
    lng: 24.25589746
  },
  {
    a: "54°44′51″N",
    lat: 54.74746474,
    b: "24°01′12″E",
    lng: 24.02009117
  },
  {
    a: "54°45′42″N",
    lat: 54.7616703,
    b: "24°10′58″E",
    lng: 24.18280042
  },
  {
    a: "54°48′38″N",
    lat: 54.8106551,
    b: "23°50′12″E",
    lng: 23.83674711
  },
  {
    a: "54°59′41″N",
    lat: 54.9947619,
    b: "23°36′45″E",
    lng: 23.61239116
  },
  {
    a: "54°54′59″N",
    lat: 54.91643171,
    b: "24°10′50″E",
    lng: 24.18042762
  },
  {
    a: "54°58′28″N",
    lat: 54.97456599,
    b: "23°41′26″E",
    lng: 23.6906405
  },
  {
    a: "54°50′53″N",
    lat: 54.84818208,
    b: "23°39′31″E",
    lng: 23.65847491
  },
  {
    a: "54°59′29″N",
    lat: 54.99131527,
    b: "23°46′29″E",
    lng: 23.77478975
  },
  {
    a: "55°04′42″N",
    lat: 55.07840083,
    b: "24°00′06″E",
    lng: 24.00153259
  },
  {
    a: "54°46′49″N",
    lat: 54.78022536,
    b: "23°53′07″E",
    lng: 23.8851708
  },
  {
    a: "54°53′42″N",
    lat: 54.89513526,
    b: "24°12′17″E",
    lng: 24.20464027
  },
  {
    a: "54°56′21″N",
    lat: 54.93915129,
    b: "23°39′10″E",
    lng: 23.65283888
  },
  {
    a: "54°58′54″N",
    lat: 54.9815396,
    b: "23°57′57″E",
    lng: 23.96578014
  },
  {
    a: "54°51′20″N",
    lat: 54.8554246,
    b: "23°57′43″E",
    lng: 23.9618496
  },
  {
    a: "54°56′23″N",
    lat: 54.93965114,
    b: "23°43′11″E",
    lng: 23.71962976
  },
  {
    a: "54°42′43″N",
    lat: 54.71207396,
    b: "24°02′01″E",
    lng: 24.03371325
  },
  {
    a: "55°02′42″N",
    lat: 55.04512689,
    b: "24°05′16″E",
    lng: 24.08766604
  },
  {
    a: "54°58′37″N",
    lat: 54.97692198,
    b: "23°49′18″E",
    lng: 23.82161123
  },
  {
    a: "54°59′31″N",
    lat: 54.9919274,
    b: "24°05′07″E",
    lng: 24.08532272
  },
  {
    a: "54°59′05″N",
    lat: 54.98474675,
    b: "24°10′24″E",
    lng: 24.17329152
  },
  {
    a: "54°45′33″N",
    lat: 54.75925275,
    b: "24°06′28″E",
    lng: 24.10785098
  },
  {
    a: "54°57′39″N",
    lat: 54.96096547,
    b: "23°38′15″E",
    lng: 23.63749234
  },
  {
    a: "55°02′48″N",
    lat: 55.04673062,
    b: "23°50′26″E",
    lng: 23.84055347
  },
  {
    a: "54°55′46″N",
    lat: 54.92949541,
    b: "24°00′06″E",
    lng: 24.00175984
  },
  {
    a: "54°42′33″N",
    lat: 54.70921042,
    b: "23°45′48″E",
    lng: 23.76325047
  },
  {
    a: "54°50′02″N",
    lat: 54.83389872,
    b: "23°38′30″E",
    lng: 23.64158264
  },
  {
    a: "54°49′05″N",
    lat: 54.81813253,
    b: "23°59′07″E",
    lng: 23.98519798
  },
  {
    a: "54°55′02″N",
    lat: 54.91713402,
    b: "23°41′13″E",
    lng: 23.68685546
  },
  {
    a: "54°49′59″N",
    lat: 54.83298017,
    b: "24°15′29″E",
    lng: 24.25802666
  },
  {
    a: "54°51′47″N",
    lat: 54.86307859,
    b: "23°37′59″E",
    lng: 23.63337252
  },
  {
    a: "54°48′33″N",
    lat: 54.80915241,
    b: "23°55′05″E",
    lng: 23.9179426
  },
  {
    a: "54°51′45″N",
    lat: 54.86244567,
    b: "23°34′42″E",
    lng: 23.57825674
  },
  {
    a: "55°05′48″N",
    lat: 55.09656783,
    b: "24°00′00″E",
    lng: 24.00011506
  },
  {
    a: "54°52′29″N",
    lat: 54.87485777,
    b: "23°38′50″E",
    lng: 23.64730602
  },
  {
    a: "54°57′33″N",
    lat: 54.95909291,
    b: "23°47′52″E",
    lng: 23.79772255
  },
  {
    a: "54°53′25″N",
    lat: 54.89021023,
    b: "24°00′29″E",
    lng: 24.00815333
  },
  {
    a: "54°51′57″N",
    lat: 54.86586888,
    b: "23°47′23″E",
    lng: 23.7896618
  },
  {
    a: "55°05′01″N",
    lat: 55.08350341,
    b: "23°48′47″E",
    lng: 23.81312191
  },
  {
    a: "54°44′18″N",
    lat: 54.73845783,
    b: "24°07′12″E",
    lng: 24.11999857
  },
  {
    a: "54°46′54″N",
    lat: 54.78161941,
    b: "24°03′43″E",
    lng: 24.06207549
  },
  {
    a: "54°52′42″N",
    lat: 54.87837215,
    b: "23°57′31″E",
    lng: 23.95874253
  },
  {
    a: "54°42′40″N",
    lat: 54.71103973,
    b: "24°01′27″E",
    lng: 24.02416513
  },
  {
    a: "54°49′52″N",
    lat: 54.83106358,
    b: "24°07′14″E",
    lng: 24.12057491
  },
  {
    a: "54°55′47″N",
    lat: 54.9296681,
    b: "23°39′21″E",
    lng: 23.65589986
  },
  {
    a: "55°01′06″N",
    lat: 55.01825134,
    b: "23°39′51″E",
    lng: 23.66425409
  },
  {
    a: "54°50′46″N",
    lat: 54.84603561,
    b: "24°08′29″E",
    lng: 24.14135747
  },
  {
    a: "54°51′19″N",
    lat: 54.85530285,
    b: "24°03′59″E",
    lng: 24.06675676
  },
  {
    a: "55°00′52″N",
    lat: 55.01433612,
    b: "24°00′11″E",
    lng: 24.00296211
  },
  {
    a: "55°03′51″N",
    lat: 55.06423149,
    b: "23°46′25″E",
    lng: 23.7736421
  },
  {
    a: "54°58′20″N",
    lat: 54.97220969,
    b: "24°10′59″E",
    lng: 24.18293732
  },
  {
    a: "55°03′57″N",
    lat: 55.06586449,
    b: "24°02′31″E",
    lng: 24.04196458
  },
  {
    a: "54°54′05″N",
    lat: 54.90133763,
    b: "23°56′14″E",
    lng: 23.937269
  },
  {
    a: "54°57′50″N",
    lat: 54.96387723,
    b: "23°50′50″E",
    lng: 23.84729303
  },
  {
    a: "54°47′26″N",
    lat: 54.79051162,
    b: "23°35′53″E",
    lng: 23.59819289
  },
  {
    a: "54°58′30″N",
    lat: 54.97510398,
    b: "24°13′15″E",
    lng: 24.22088175
  },
  {
    a: "54°49′08″N",
    lat: 54.81887154,
    b: "23°40′16″E",
    lng: 23.67109207
  },
  {
    a: "54°44′58″N",
    lat: 54.74935629,
    b: "23°43′05″E",
    lng: 23.71794211
  },
  {
    a: "54°43′54″N",
    lat: 54.73175847,
    b: "24°01′24″E",
    lng: 24.0232012
  },
  {
    a: "54°58′54″N",
    lat: 54.98152871,
    b: "24°04′31″E",
    lng: 24.07524636
  },
  {
    a: "55°06′06″N",
    lat: 55.10161105,
    b: "23°51′42″E",
    lng: 23.8615535
  },
  {
    a: "55°03′56″N",
    lat: 55.06549904,
    b: "23°58′08″E",
    lng: 23.96886803
  },
  {
    a: "54°57′24″N",
    lat: 54.95680383,
    b: "23°59′17″E",
    lng: 23.9881552
  },
  {
    a: "54°57′25″N",
    lat: 54.9568591,
    b: "23°54′40″E",
    lng: 23.91105147
  },
  {
    a: "54°41′55″N",
    lat: 54.69854706,
    b: "23°50′21″E",
    lng: 23.83912409
  }
];

var liban = [
  {
    a: "33°47′08″N",
    lat: 33.78545504,
    b: "35°25′39″E",
    lng: 35.42760037
  },
  {
    a: "33°48′43″N",
    lat: 33.81189647,
    b: "35°31′35″E",
    lng: 35.52644725
  },
  {
    a: "33°52′37″N",
    lat: 33.87704389,
    b: "35°24′19″E",
    lng: 35.40527592
  },
  {
    a: "33°55′14″N",
    lat: 33.92052177,
    b: "35°23′04″E",
    lng: 35.38452231
  },
  {
    a: "33°49′17″N",
    lat: 33.82130203,
    b: "35°37′28″E",
    lng: 35.6244604
  },
  {
    a: "33°52′44″N",
    lat: 33.87897873,
    b: "35°36′03″E",
    lng: 35.60093924
  },
  {
    a: "33°49′40″N",
    lat: 33.8277338,
    b: "35°29′36″E",
    lng: 35.49336423
  },
  {
    a: "33°48′04″N",
    lat: 33.80108827,
    b: "35°35′35″E",
    lng: 35.59297664
  },
  {
    a: "33°55′08″N",
    lat: 33.9188557,
    b: "35°35′21″E",
    lng: 35.58924839
  },
  {
    a: "33°57′57″N",
    lat: 33.96579733,
    b: "35°34′11″E",
    lng: 35.56984448
  },
  {
    a: "33°57′40″N",
    lat: 33.96104837,
    b: "35°33′27″E",
    lng: 35.55741218
  },
  {
    a: "33°58′02″N",
    lat: 33.96719223,
    b: "35°31′32″E",
    lng: 35.52565844
  },
  {
    a: "33°48′40″N",
    lat: 33.81111423,
    b: "35°22′48″E",
    lng: 35.38008012
  },
  {
    a: "33°50′11″N",
    lat: 33.83627027,
    b: "35°38′11″E",
    lng: 35.6365107
  },
  {
    a: "33°56′18″N",
    lat: 33.93835607,
    b: "35°35′31″E",
    lng: 35.59182272
  },
  {
    a: "33°51′17″N",
    lat: 33.85462112,
    b: "35°24′18″E",
    lng: 35.4051165
  },
  {
    a: "33°55′21″N",
    lat: 33.92236993,
    b: "35°31′27″E",
    lng: 35.52408526
  },
  {
    a: "33°56′24″N",
    lat: 33.94011235,
    b: "35°26′39″E",
    lng: 35.4441802
  },
  {
    a: "33°55′07″N",
    lat: 33.91850863,
    b: "35°36′24″E",
    lng: 35.60654867
  },
  {
    a: "33°53′02″N",
    lat: 33.8838131,
    b: "35°25′43″E",
    lng: 35.42860627
  },
  {
    a: "33°52′23″N",
    lat: 33.87300349,
    b: "35°24′12″E",
    lng: 35.40319802
  },
  {
    a: "33°58′33″N",
    lat: 33.97577602,
    b: "35°25′03″E",
    lng: 35.41752837
  },
  {
    a: "33°54′19″N",
    lat: 33.9052027,
    b: "35°28′17″E",
    lng: 35.4712826
  },
  {
    a: "33°55′58″N",
    lat: 33.93283074,
    b: "35°27′42″E",
    lng: 35.46174236
  },
  {
    a: "33°50′52″N",
    lat: 33.84787366,
    b: "35°28′03″E",
    lng: 35.46749181
  },
  {
    a: "33°59′49″N",
    lat: 33.99690686,
    b: "35°33′11″E",
    lng: 35.55297197
  },
  {
    a: "33°48′01″N",
    lat: 33.8002998,
    b: "35°32′12″E",
    lng: 35.53678081
  },
  {
    a: "34°01′12″N",
    lat: 34.02013687,
    b: "35°31′14″E",
    lng: 35.52053249
  },
  {
    a: "33°53′12″N",
    lat: 33.88678619,
    b: "35°31′29″E",
    lng: 35.52477155
  },
  {
    a: "33°54′13″N",
    lat: 33.90355285,
    b: "35°21′46″E",
    lng: 35.36265329
  },
  {
    a: "33°53′28″N",
    lat: 33.8911786,
    b: "35°24′22″E",
    lng: 35.40605899
  },
  {
    a: "33°51′31″N",
    lat: 33.85861019,
    b: "35°31′01″E",
    lng: 35.51703339
  },
  {
    a: "33°53′15″N",
    lat: 33.88763188,
    b: "35°24′59″E",
    lng: 35.41653744
  },
  {
    a: "33°52′30″N",
    lat: 33.87496188,
    b: "35°27′21″E",
    lng: 35.45575023
  },
  {
    a: "33°59′55″N",
    lat: 33.99857695,
    b: "35°27′19″E",
    lng: 35.45514832
  },
  {
    a: "34°00′12″N",
    lat: 34.00342661,
    b: "35°35′30″E",
    lng: 35.59155328
  },
  {
    a: "33°53′49″N",
    lat: 33.89706752,
    b: "35°37′34″E",
    lng: 35.62608244
  },
  {
    a: "33°46′38″N",
    lat: 33.7770909,
    b: "35°32′15″E",
    lng: 35.53740872
  },
  {
    a: "33°57′17″N",
    lat: 33.95478437,
    b: "35°22′11″E",
    lng: 35.36976829
  },
  {
    a: "33°51′38″N",
    lat: 33.86051724,
    b: "35°31′48″E",
    lng: 35.53005109
  },
  {
    a: "33°52′23″N",
    lat: 33.87309456,
    b: "35°31′50″E",
    lng: 35.53054146
  },
  {
    a: "33°53′42″N",
    lat: 33.89495454,
    b: "35°25′05″E",
    lng: 35.41801035
  },
  {
    a: "33°56′36″N",
    lat: 33.94325123,
    b: "35°35′50″E",
    lng: 35.59710115
  },
  {
    a: "33°48′43″N",
    lat: 33.81187418,
    b: "35°37′56″E",
    lng: 35.63214881
  },
  {
    a: "33°53′50″N",
    lat: 33.89731639,
    b: "35°27′33″E",
    lng: 35.45928451
  },
  {
    a: "33°46′32″N",
    lat: 33.77548835,
    b: "35°26′25″E",
    lng: 35.44039818
  },
  {
    a: "33°57′06″N",
    lat: 33.95152815,
    b: "35°23′02″E",
    lng: 35.38390085
  },
  {
    a: "33°53′08″N",
    lat: 33.88542096,
    b: "35°21′20″E",
    lng: 35.35545779
  },
  {
    a: "33°57′37″N",
    lat: 33.96036238,
    b: "35°28′40″E",
    lng: 35.47776219
  },
  {
    a: "33°52′49″N",
    lat: 33.8803274,
    b: "35°30′10″E",
    lng: 35.50285368
  },
  {
    a: "33°54′51″N",
    lat: 33.91425058,
    b: "35°33′19″E",
    lng: 35.55532304
  },
  {
    a: "33°56′18″N",
    lat: 33.93830428,
    b: "35°28′05″E",
    lng: 35.46793798
  },
  {
    a: "33°57′09″N",
    lat: 33.95240054,
    b: "35°35′17″E",
    lng: 35.58802678
  },
  {
    a: "33°50′18″N",
    lat: 33.83843445,
    b: "35°24′13″E",
    lng: 35.4037151
  },
  {
    a: "33°50′48″N",
    lat: 33.84676456,
    b: "35°28′24″E",
    lng: 35.47334596
  },
  {
    a: "33°58′22″N",
    lat: 33.97276365,
    b: "35°35′35″E",
    lng: 35.59313962
  },
  {
    a: "33°55′18″N",
    lat: 33.92162579,
    b: "35°34′39″E",
    lng: 35.57738435
  },
  {
    a: "33°49′07″N",
    lat: 33.8187194,
    b: "35°26′44″E",
    lng: 35.44556877
  },
  {
    a: "33°50′53″N",
    lat: 33.84791892,
    b: "35°25′23″E",
    lng: 35.42308049
  },
  {
    a: "33°49′39″N",
    lat: 33.82754874,
    b: "35°22′47″E",
    lng: 35.37971465
  },
  {
    a: "33°57′28″N",
    lat: 33.95786025,
    b: "35°24′20″E",
    lng: 35.40542438
  },
  {
    a: "33°54′38″N",
    lat: 33.91065741,
    b: "35°36′20″E",
    lng: 35.60553412
  },
  {
    a: "33°52′55″N",
    lat: 33.88182695,
    b: "35°34′51″E",
    lng: 35.58086448
  },
  {
    a: "33°57′17″N",
    lat: 33.95472309,
    b: "35°26′14″E",
    lng: 35.43726907
  },
  {
    a: "33°47′34″N",
    lat: 33.79279163,
    b: "35°25′59″E",
    lng: 35.433228
  },
  {
    a: "33°48′30″N",
    lat: 33.80824666,
    b: "35°27′45″E",
    lng: 35.46253377
  },
  {
    a: "33°53′47″N",
    lat: 33.89629595,
    b: "35°29′02″E",
    lng: 35.48382853
  },
  {
    a: "33°56′36″N",
    lat: 33.94344178,
    b: "35°23′26″E",
    lng: 35.39063419
  },
  {
    a: "33°52′08″N",
    lat: 33.86886757,
    b: "35°25′40″E",
    lng: 35.42767496
  },
  {
    a: "33°55′23″N",
    lat: 33.92308103,
    b: "35°34′29″E",
    lng: 35.57470035
  },
  {
    a: "33°47′58″N",
    lat: 33.79956225,
    b: "35°32′49″E",
    lng: 35.54697714
  },
  {
    a: "33°56′09″N",
    lat: 33.93594448,
    b: "35°32′38″E",
    lng: 35.54384362
  },
  {
    a: "33°59′47″N",
    lat: 33.99643548,
    b: "35°26′01″E",
    lng: 35.43356011
  },
  {
    a: "33°59′19″N",
    lat: 33.98854467,
    b: "35°24′59″E",
    lng: 35.41667246
  },
  {
    a: "33°52′20″N",
    lat: 33.87223518,
    b: "35°21′45″E",
    lng: 35.36239333
  },
  {
    a: "33°48′11″N",
    lat: 33.80314663,
    b: "35°37′40″E",
    lng: 35.62772689
  },
  {
    a: "33°54′26″N",
    lat: 33.9071079,
    b: "35°31′21″E",
    lng: 35.52250797
  },
  {
    a: "33°46′01″N",
    lat: 33.76696429,
    b: "35°31′26″E",
    lng: 35.52378913
  },
  {
    a: "33°54′38″N",
    lat: 33.91060591,
    b: "35°23′44″E",
    lng: 35.395448
  },
  {
    a: "33°52′20″N",
    lat: 33.87211812,
    b: "35°30′09″E",
    lng: 35.50241033
  },
  {
    a: "33°46′56″N",
    lat: 33.78214848,
    b: "35°30′38″E",
    lng: 35.51042381
  },
  {
    a: "33°51′13″N",
    lat: 33.85374156,
    b: "35°21′51″E",
    lng: 35.36429645
  },
  {
    a: "33°49′24″N",
    lat: 33.82334302,
    b: "35°38′06″E",
    lng: 35.63510397
  },
  {
    a: "33°52′06″N",
    lat: 33.86838967,
    b: "35°36′59″E",
    lng: 35.61638279
  },
  {
    a: "33°48′05″N",
    lat: 33.80137505,
    b: "35°24′56″E",
    lng: 35.41556013
  },
  {
    a: "33°50′20″N",
    lat: 33.8390241,
    b: "35°28′27″E",
    lng: 35.47416329
  },
  {
    a: "33°48′07″N",
    lat: 33.80206925,
    b: "35°33′13″E",
    lng: 35.55374794
  },
  {
    a: "33°48′52″N",
    lat: 33.8143468,
    b: "35°25′25″E",
    lng: 35.42361634
  },
  {
    a: "33°52′37″N",
    lat: 33.87684807,
    b: "35°27′25″E",
    lng: 35.45707148
  },
  {
    a: "33°53′02″N",
    lat: 33.88377509,
    b: "35°26′32″E",
    lng: 35.4423273
  },
  {
    a: "33°57′33″N",
    lat: 33.95928407,
    b: "35°32′15″E",
    lng: 35.53745636
  },
  {
    a: "33°51′40″N",
    lat: 33.86102235,
    b: "35°36′41″E",
    lng: 35.61152639
  },
  {
    a: "33°54′55″N",
    lat: 33.91537296,
    b: "35°23′06″E",
    lng: 35.38512137
  },
  {
    a: "33°57′50″N",
    lat: 33.96396105,
    b: "35°27′53″E",
    lng: 35.46473419
  },
  {
    a: "33°53′30″N",
    lat: 33.89171003,
    b: "35°22′49″E",
    lng: 35.38021034
  },
  {
    a: "33°56′36″N",
    lat: 33.94342118,
    b: "35°29′15″E",
    lng: 35.48741379
  },
  {
    a: "33°55′27″N",
    lat: 33.9243036,
    b: "35°37′21″E",
    lng: 35.62245147
  },
  {
    a: "33°55′47″N",
    lat: 33.92963299,
    b: "35°39′10″E",
    lng: 35.65277655
  },
  {
    a: "33°46′52″N",
    lat: 33.78111612,
    b: "35°33′48″E",
    lng: 35.56343322
  },
  {
    a: "33°58′04″N",
    lat: 33.96776069,
    b: "35°23′11″E",
    lng: 35.38643367
  }
];

var bars = [
  { id: "2e4c4e55-79d5-498d-93b2-925959818fc7", name: "Photojam", rating: 15 },
  { id: "ad0cda1f-0233-4e39-9887-e914d48138be", name: "Wordtune", rating: 90 },
  { id: "785ac17e-5424-4afb-8901-2ae6376d609b", name: "Yotz", rating: 62 },
  { id: "02a41ba5-5ba0-4c83-aa9f-4ac4496ea633", name: "Geba", rating: 13 },
  { id: "2ed2df85-436b-4030-8508-2e42d965793b", name: "Innotype", rating: 36 },
  { id: "02a507c5-4c53-485e-bf27-5cd0a3066b29", name: "Jayo", rating: 11 },
  { id: "c7cc53ec-90a6-4ab1-9d4c-d2cf49183394", name: "Devcast", rating: 80 },
  { id: "bb4ca435-43db-49e7-9a2b-28f918ab0eff", name: "Yodoo", rating: 100 },
  { id: "bd7232ad-c73f-4517-a259-a142b26d9c21", name: "Eayo", rating: 98 },
  { id: "c658f9d9-d8ea-44f7-93fe-43076c311bd1", name: "Camido", rating: 47 },
  { id: "e27dcfec-b742-4b8b-8ac1-ee3feb81b5ed", name: "Skipfire", rating: 18 },
  { id: "3f172956-a1b0-4702-b829-96f3568660ee", name: "Jaxbean", rating: 94 },
  {
    id: "07cbf507-4fbf-4aa9-b6b0-5671baa63e2d",
    name: "Twitterwire",
    rating: 75
  },
  {
    id: "0a70ad65-071d-4198-b770-3cdb5d4cb910",
    name: "Jabberstorm",
    rating: 11
  },
  { id: "dbff9806-55e8-4369-9733-456c4d7d9986", name: "Mynte", rating: 42 },
  { id: "a3e31fdf-2b29-4413-a44d-cf0eb9785ab6", name: "Bluezoom", rating: 26 },
  { id: "0285732f-fe71-41af-92a9-391db1b014f7", name: "Eayo", rating: 1 },
  {
    id: "7b5c69c9-b65c-4e70-b269-aa2519e78924",
    name: "Topiclounge",
    rating: 1
  },
  { id: "3119a63b-fec6-4af7-9fad-d8004bcae9df", name: "Flipstorm", rating: 12 },
  { id: "223c9d81-d693-4098-bade-f374835f152d", name: "Livefish", rating: 56 },
  { id: "f59ec95f-aa72-4ddb-a4b1-2e721045d6eb", name: "Miboo", rating: 13 },
  { id: "26ac0007-4151-4511-8fc6-e7a7bf465945", name: "Devify", rating: 20 },
  { id: "fb602f12-9247-4b4d-b1ca-21602960b81e", name: "Riffwire", rating: 93 },
  { id: "956d1e54-8b45-4057-9e49-230e74a92933", name: "Edgeify", rating: 15 },
  { id: "f69c4278-d15b-4944-9df2-a329c94c7712", name: "Ainyx", rating: 45 },
  { id: "99c32430-3c7a-44f1-85cd-336d8255f320", name: "Kwimbee", rating: 73 },
  { id: "308f4063-9568-4fdf-8d77-f6ab09b93e84", name: "Mynte", rating: 96 },
  { id: "04d6b024-781d-4c7f-bdf3-0cb6a109fbfb", name: "Skyvu", rating: 20 },
  { id: "d50564cc-94db-4c3b-90ab-a40dabdeee6d", name: "Feedspan", rating: 54 },
  { id: "9262e039-deab-4bc2-81e1-4b451bc33543", name: "Blogpad", rating: 42 },
  { id: "62a4deba-f1d4-4610-9811-733be4686b4b", name: "Gigazoom", rating: 6 },
  { id: "b5465097-c981-4a65-b0f1-1897b683ec39", name: "Wikibox", rating: 87 },
  { id: "fd97c495-8bb1-4b64-8344-0aa3b619be43", name: "Quinu", rating: 65 },
  { id: "70732e82-941e-4d75-81df-1ca0d83f986f", name: "Wordtune", rating: 99 },
  { id: "2a961315-5a32-4177-9912-77f1526c8f75", name: "Jaloo", rating: 61 },
  { id: "62323e25-a8b0-4b26-bfed-96466272f2a2", name: "Fivebridge", rating: 6 },
  { id: "4e45d5b4-50c2-47c5-944f-77791fb6540e", name: "Skipstorm", rating: 32 },
  { id: "e8b48d56-e947-4c9a-aa5b-93a3ccf9f682", name: "Rhybox", rating: 63 },
  { id: "3b8c20d0-e252-4fd1-ad22-19088be9fe72", name: "Jayo", rating: 93 },
  { id: "19935832-d5eb-4252-9f22-d2668c7c98be", name: "Snaptags", rating: 26 },
  { id: "4a9641d6-ff28-4ac1-9fbc-8e5a4cd0fe92", name: "Twinte", rating: 36 },
  { id: "9803bc3e-7b7b-4309-97a0-558178dbc263", name: "Skyndu", rating: 78 },
  { id: "fce8a392-6ceb-44a4-81b4-db2906a9fee4", name: "Flashspan", rating: 7 },
  { id: "759252db-fe24-4337-8d52-4dc2ab56c8c3", name: "Quatz", rating: 42 },
  { id: "8b44c991-72b5-4e09-97f0-26bd0effb651", name: "Kimia", rating: 21 },
  {
    id: "9c52ce50-01d1-4760-b2f0-26e1c7569aa5",
    name: "Chatterpoint",
    rating: 26
  },
  { id: "88e25b94-cc8c-4eeb-9bee-2bdb28475534", name: "Divavu", rating: 61 },
  { id: "e24ce117-cdc5-4713-8989-bedc97eadc84", name: "Myworks", rating: 9 },
  { id: "fa9191c4-179e-4a73-8614-8b0edef0581f", name: "Linktype", rating: 21 },
  { id: "42edf35f-15fa-41df-8bb2-9b4f8a289938", name: "Riffpedia", rating: 21 },
  { id: "a042abbc-e080-47c1-ab03-0e5053bd9d0e", name: "Linktype", rating: 33 },
  { id: "8c11172e-2584-4cf2-bec2-06ba94dacbd9", name: "Eire", rating: 9 },
  {
    id: "87cce894-554a-4f49-90bd-eec781730a13",
    name: "Flashpoint",
    rating: 43
  },
  { id: "8aa75b9a-9ac5-4f57-8d2f-0d331858cbd9", name: "Einti", rating: 12 },
  { id: "c74f5733-ab5d-44a7-9725-970cfb249c5a", name: "Fiveclub", rating: 31 },
  { id: "1e6c984e-9706-43a1-a22a-89727b2a4403", name: "Devshare", rating: 70 },
  { id: "7a8d89e9-b663-4504-9680-e51d3f7284eb", name: "Skiba", rating: 55 },
  { id: "05041c01-90ba-44af-b575-ff08e4250da9", name: "Yombu", rating: 16 },
  { id: "fa71873b-62f2-4213-841f-fd88ab6ae00f", name: "Npath", rating: 12 },
  { id: "4aea6850-cb25-4e35-843d-5d8165a3b66f", name: "Avamba", rating: 19 },
  { id: "8f6fc64e-8aaf-48cd-b948-83e1950f3fa2", name: "Rhybox", rating: 95 },
  { id: "4a2e253c-56f9-4167-9df9-26aa7056510a", name: "Cogilith", rating: 4 },
  {
    id: "d7ee78d8-563d-4ec2-a1db-92ee59429019",
    name: "Twitternation",
    rating: 48
  },
  {
    id: "fed34777-3d60-426d-b60d-27657e09c5c7",
    name: "Divanoodle",
    rating: 22
  },
  { id: "ad137704-9853-4bfb-bff0-40332e926321", name: "Brainbox", rating: 35 },
  {
    id: "a191c99a-6e8b-4824-b99f-ada922f89d08",
    name: "Babbleblab",
    rating: 77
  },
  { id: "38e8fd44-8826-4cb4-a5e8-bfa9ffdc0333", name: "Devshare", rating: 83 },
  { id: "c3daf02c-5b5d-491a-bbc0-b31741c60a5c", name: "Centidel", rating: 84 },
  { id: "66ff8a40-891b-46d4-9c95-e07b76fc6254", name: "Yodo", rating: 18 },
  { id: "72e284cf-ed3a-47e7-bd01-31a2d5cf2a25", name: "Realmix", rating: 35 },
  { id: "345b630b-2fd7-4364-ac7b-86e2330a1549", name: "Livetube", rating: 40 },
  { id: "60157105-1091-42de-b12c-8cfa1713c5f2", name: "Youtags", rating: 75 },
  { id: "f0c10664-547e-4521-b1ba-a14f863ff3ff", name: "Mydo", rating: 71 },
  { id: "fa0eb8ee-1a59-400e-bb35-75f87f93e226", name: "Skiptube", rating: 12 },
  { id: "6c0541d5-6007-48bb-8790-37180ab7cab8", name: "Tekfly", rating: 87 },
  { id: "0c81a0af-ca99-46cf-b19f-3d37e303fb55", name: "Trudeo", rating: 98 },
  { id: "a630b4c2-0e11-4ba9-ac64-ff1caedc6463", name: "Feedmix", rating: 75 },
  { id: "dcd266d9-a021-4cf8-b3c2-2819e368c4ea", name: "Leexo", rating: 13 },
  { id: "4326df2e-e7b9-4ec6-b7d2-a8d8f867ace5", name: "Jetwire", rating: 70 },
  { id: "11c1a15e-1624-4552-b5f6-de6f3f26a949", name: "Skyba", rating: 85 },
  { id: "9f917865-8606-4dce-9a5e-414cd1a831b2", name: "Ainyx", rating: 65 },
  { id: "f45fad30-9d4e-49ae-a8cb-8ed426e389b4", name: "Devshare", rating: 46 },
  { id: "cc3a9d27-75f0-4db0-81bd-0ecb211db78c", name: "Centidel", rating: 18 },
  {
    id: "f88107ae-5a07-4cef-af4d-f920fe2e4bad",
    name: "Twitterbeat",
    rating: 12
  },
  {
    id: "25fafba4-73e0-47ad-8db5-b1d90386ecf7",
    name: "Thoughtblab",
    rating: 95
  },
  {
    id: "92bd835c-7252-4348-a14e-b0cc30883cb9",
    name: "Topiclounge",
    rating: 54
  },
  { id: "361e925d-cc1e-4d2d-a8a1-8ec0601d762d", name: "Dynava", rating: 23 },
  { id: "4453a606-ca26-40c0-a2ba-1f4c5ad074d1", name: "Skilith", rating: 28 },
  { id: "0610c23a-63eb-490f-a488-4e1dc64cf354", name: "Aibox", rating: 91 },
  { id: "d03da5e6-8d41-472b-8ed3-6c08d16fe36c", name: "Tambee", rating: 33 },
  { id: "d7b50941-0e73-4620-b90b-67b22122d136", name: "Realfire", rating: 72 },
  { id: "7b3930da-2fb3-407c-86ac-adcdaf6c91b6", name: "Rhyloo", rating: 38 },
  { id: "2ac16647-1167-4158-9c81-37135d8e6ff5", name: "Teklist", rating: 66 },
  { id: "2dd1ff6d-1c51-45d5-9c5d-40b8730340e9", name: "Livefish", rating: 77 },
  { id: "f55efe0a-2b04-4ac6-8872-ac48b6c23906", name: "Kamba", rating: 89 },
  { id: "03408f46-3a18-452c-90b4-7c7219696792", name: "Tavu", rating: 20 },
  { id: "54b2f7e2-e41f-4d6e-9836-9ab7411cd2c2", name: "Yozio", rating: 77 },
  { id: "8ad81eb1-3aeb-441b-890d-71cf2ea38a08", name: "Wordpedia", rating: 66 },
  { id: "3c41cbc8-ba0f-415c-816c-983c86fe0155", name: "Youopia", rating: 22 },
  { id: "7ccdb438-e67c-40ba-b37f-ff9eb3637713", name: "Roombo", rating: 18 }
];

var beers = [
  {
    id: "2538e5eb-7442-4c8b-81d9-8e825200dad2",
    name: "Boogie (Boogie, el aceitoso)",
    rating: 97,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "e0a8d76b-bfb5-4ea8-9182-647f0490b7ac",
    name: "Summer School",
    rating: 34,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "a1cc25c8-623c-40de-a0c2-3f655765f871",
    name: "Final Fantasy VII: Advent Children",
    rating: 82,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "ef22b6e8-03af-4a11-a860-888fc641166a",
    name: "Mother and Child",
    rating: 47,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "fbc75509-4c2c-4d2b-be17-e2f664d0742e",
    name: "My Perestroika",
    rating: 69,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "299ed961-4d1a-49e2-bef8-eaceea72deb9",
    name: "Petulia",
    rating: 22,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "54593a9f-f705-4b9b-8ff7-dba96fd5b410",
    name: "On the Outs",
    rating: 75,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "81caecbf-1567-445d-b396-39dc0f083228",
    name: "Wreck-It Ralph",
    rating: 29,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "03a197c9-29c4-422a-ae96-e52b168de702",
    name: "These Three",
    rating: 60,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "10db112f-353e-40c4-92d5-368a075f5612",
    name: "Soundbreaker ",
    rating: 48,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "afe48330-5f5b-4e99-b8b8-28caa7856777",
    name: "Liar Liar",
    rating: 56,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "de40f757-1cc8-47b9-aafc-e8450f27b41b",
    name: "The Evictors",
    rating: 28,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "c18a9337-1cc8-4412-803b-4ddbce1187f8",
    name: "Elephant in the Living Room, The",
    rating: 96,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "c4cafe80-cff5-4c91-99d2-9a1f1c37dc9c",
    name: "Naked Civil Servant, The",
    rating: 79,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "123e4475-829c-4594-aaa6-5076be56ebcb",
    name: "C.H.U.D.",
    rating: 99,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "652a237c-af44-49bf-9b9a-08e0bdb57c8e",
    name: "What Is It?",
    rating: 64,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "89bfdfd4-e5bb-4f8e-9a6b-b15fb6070e61",
    name: "Blow-Out (La grande bouffe)",
    rating: 37,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "dbafaf8a-d640-4740-8a5c-b60354aabff1",
    name: "Absolute Beginners",
    rating: 61,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "f7bb3efc-b40e-48b9-b5c6-5b29f884de39",
    name: "Next Stop, Greenwich Village",
    rating: 80,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "a0ec9786-1e3a-4834-8ef1-dd73ce5c733a",
    name: "Debt, The",
    rating: 70,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "6a74ec89-4d3c-4d2b-9749-23b14d1d6a61",
    name: "First Blood (Rambo: First Blood)",
    rating: 83,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "02466cda-c7b2-4769-aaeb-d9e8cc00e68a",
    name: "Chato's Land",
    rating: 59,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "2727c947-95af-4ae8-b724-bbb4086cc3a2",
    name: "Angel Named Billy, An",
    rating: 73,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "6d36d178-f565-4cfe-aa0b-9904ccf93976",
    name: "Leave The World Behind",
    rating: 51,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "3c364c20-bdf0-4e74-9410-17df5c3bf6a8",
    name: "Medallion, The",
    rating: 88,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "b82a9124-dc4c-4f2a-9689-5b704f910f58",
    name: "Nickelodeon",
    rating: 83,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "95f33891-aed4-46ab-b8b7-661a50391898",
    name: "Chalte Chalte",
    rating: 71,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "09f2af01-238b-41bb-84bd-e97b299751eb",
    name: "Amer",
    rating: 87,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "d1d0550d-8446-4eb1-a8a1-53e168c8d538",
    name: "Distant Voices, Still Lives",
    rating: 31,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  },
  {
    id: "4444e642-8ce3-422d-ab36-77163d5a986c",
    name: "Document of the Dead",
    rating: 22,
    icon: "https://via.placeholder.com/58x58",
    image: "https://via.placeholder.com/69x250"
  }
];

var bartenders = [
  {
    id: 1,
    first_name: "Edy",
    last_name: "Pescod",
    image: "https://via.placeholder.com/100x100",
    rating: 3.2
  },
  {
    id: 2,
    first_name: "Marieann",
    last_name: "Sillwood",
    image: "https://via.placeholder.com/100x100",
    rating: 1.4
  },
  {
    id: 3,
    first_name: "Leone",
    last_name: "Lygo",
    image: "https://via.placeholder.com/100x100",
    rating: 2.9
  },
  {
    id: 4,
    first_name: "Ichabod",
    last_name: "Ilem",
    image: "https://via.placeholder.com/100x100",
    rating: 2.8
  },
  {
    id: 5,
    first_name: "Mata",
    last_name: "Taffee",
    image: "https://via.placeholder.com/100x100",
    rating: 2.0
  },
  {
    id: 6,
    first_name: "Maren",
    last_name: "Dillistone",
    image: "https://via.placeholder.com/100x100",
    rating: 1.8
  },
  {
    id: 7,
    first_name: "Jenica",
    last_name: "Caughan",
    image: "https://via.placeholder.com/100x100",
    rating: 3.5
  },
  {
    id: 8,
    first_name: "Orton",
    last_name: "Atcherley",
    image: "https://via.placeholder.com/100x100",
    rating: 3.9
  },
  {
    id: 9,
    first_name: "Noni",
    last_name: "Dunkley",
    image: "https://via.placeholder.com/100x100",
    rating: 1.4
  },
  {
    id: 10,
    first_name: "Koral",
    last_name: "Woodall",
    image: "https://via.placeholder.com/100x100",
    rating: 3.8
  },
  {
    id: 11,
    first_name: "Kasey",
    last_name: "O'Dunniom",
    image: "https://via.placeholder.com/100x100",
    rating: 4.2
  },
  {
    id: 12,
    first_name: "Leanna",
    last_name: "Gowman",
    image: "https://via.placeholder.com/100x100",
    rating: 1.2
  },
  {
    id: 13,
    first_name: "Ellyn",
    last_name: "Behnecke",
    image: "https://via.placeholder.com/100x100",
    rating: 3.0
  },
  {
    id: 14,
    first_name: "Mort",
    last_name: "Manicomb",
    image: "https://via.placeholder.com/100x100",
    rating: 3.3
  },
  {
    id: 15,
    first_name: "Arvy",
    last_name: "Rawet",
    image: "https://via.placeholder.com/100x100",
    rating: 4.6
  },
  {
    id: 16,
    first_name: "Bren",
    last_name: "Hacksby",
    image: "https://via.placeholder.com/100x100",
    rating: 1.0
  },
  {
    id: 17,
    first_name: "Giffard",
    last_name: "Burchatt",
    image: "https://via.placeholder.com/100x100",
    rating: 1.6
  },
  {
    id: 18,
    first_name: "Ogdan",
    last_name: "Gredden",
    image: "https://via.placeholder.com/100x100",
    rating: 4.8
  },
  {
    id: 19,
    first_name: "Issie",
    last_name: "Cromley",
    image: "https://via.placeholder.com/100x100",
    rating: 2.2
  },
  {
    id: 20,
    first_name: "Rey",
    last_name: "Ogglebie",
    image: "https://via.placeholder.com/100x100",
    rating: 4.7
  },
  {
    id: 21,
    first_name: "Jone",
    last_name: "Burg",
    image: "https://via.placeholder.com/100x100",
    rating: 3.9
  },
  {
    id: 22,
    first_name: "Berti",
    last_name: "Elves",
    image: "https://via.placeholder.com/100x100",
    rating: 1.9
  },
  {
    id: 23,
    first_name: "Adora",
    last_name: "Jacques",
    image: "https://via.placeholder.com/100x100",
    rating: 2.4
  },
  {
    id: 24,
    first_name: "Salim",
    last_name: "Moulsdale",
    image: "https://via.placeholder.com/100x100",
    rating: 4.9
  },
  {
    id: 25,
    first_name: "Wilburt",
    last_name: "Angric",
    image: "https://via.placeholder.com/100x100",
    rating: 4.8
  },
  {
    id: 26,
    first_name: "Rana",
    last_name: "Merrgen",
    image: "https://via.placeholder.com/100x100",
    rating: 4.4
  },
  {
    id: 27,
    first_name: "Donall",
    last_name: "Sier",
    image: "https://via.placeholder.com/100x100",
    rating: 1.6
  },
  {
    id: 28,
    first_name: "Dionis",
    last_name: "Tomsett",
    image: "https://via.placeholder.com/100x100",
    rating: 1.1
  },
  {
    id: 29,
    first_name: "Essa",
    last_name: "MacTrustam",
    image: "https://via.placeholder.com/100x100",
    rating: 1.4
  },
  {
    id: 30,
    first_name: "Rolf",
    last_name: "Polden",
    image: "https://via.placeholder.com/100x100",
    rating: 1.5
  }
];

var barsContructorFunction = (r, index) => {
  return {
    id: bars[index].id,
    name: bars[index].name,
    rating: bars[index].rating,
    lat: r.lat,
    lng: r.lng,
    servedBeers: getRandom(beers, Math.floor(Math.random() * 10) + 1).map(v => {
      return v.id;
    }),
    bartenders: getRandom(bartenders, Math.floor(Math.random() * 4) + 1).map(
      v => {
        return v.id;
      }
    )
  };
};

module.exports = function() {
  var data = {};

  var dataPoints = [
    { locationKey: "beirut-governorate", locations: liban },
    { locationKey: "kauno-apskr", locations: kaunas }
  ];

  data.location = dataPoints.map(d => {
    return { id: d.locationKey, bars: d.locations.map(barsContructorFunction) };
  });

  data.bartenders = bartenders;
  data.beers = beers;

  return data;
};

function getRandom(arr, n) {
  var result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len)
    throw new RangeError("getRandom: more elements taken than available");
  while (n--) {
    var x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}
